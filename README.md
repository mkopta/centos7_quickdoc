# Centos 7 quickdoc

Learn how to work with systemd, firewalld, selinux and what is new in CentOS 7.

## Agenda

* What is new in CentOS 7
* systemd
  * 101 - basic commands to operate the system
  * Comparison with init.
  * Unit files and how to create one.
  * Configuring systemd.
* FirewallD
* SELinux
* Tips and tricks

## What is new in CentOS 7


### What has changed

|  | Before | After |
|---|---|---|
| Init (PID 1) | init | systemd |
| System and service manger | upstart | systemd |
| Default FS | ext4 | xfs |
| Kernel | 2.6 | 3.10 |
| Default first user id | 500 | 1000 |
| Maximum file size | 16 TB | 500 TB |
| Directories | `/{bin,sbin,lib,lib64}` | `/usr/{bin,sbin,lib,lib64}` |
| Default boot loader | GRUB 0.97 | GRUB 2  _(GPT, EFI, xfs, ext4, ntfs, hfs+, raid, ..)_ |
| Default firewall | iptables | FirewallD |
| Default desktop | GNOME 2 | GNOME 3, KDE 4.10 |
| Default database | MySQL | MariaDB |
| Default java | OpenJDK 1.6 | OpenJDK 1.7 |

### What is new

* Docker support (docker available in extras).
* Atomic host (bare installation to support containers).
* No 32 bit support anymore.
* Python 3 available in EPEL.
* Support for 40 Gigabit NICs.
* Minium 1GB (5GB) to install.
* Support for new processors (Intel Broadwell) and graphics (AMD Hawaii).
* Full support for OpenJDK 1.8.



## systemd

### 101 - basic commands to operate the system


Basic service operations
```
systemctl disable sshd
systemctl enable sshd
systemctl start sshd
systemctl stop sshd
systemctl restart sshd
systemctl reload sshd
systemctl status sshd
```

Basic log operations

* See all logs & go to end: `journalctl -e`
* Follow tail of all logs: `journalctl -f`
* Logs limited to this boot: `journalctl -b`
* Logs limited to specific service `journalctl -u sshd`


### Comparison with init


* C versus shell scripts versus
* Unit files versus init scripts
* Targets versus runlevels
* Binary versus plain text logs
* systemd never loses initial log messages
* systemd knows when a service crashes
* systemd can respawn daemons as needed
* systemd records runtime data (captures stdout/stderr of processes)
* systemd doesn't lose daemon context during runtime
* systemd can kill all components of a service cleanly
* systemctl command replaces `service` and `chkconfig`

### Comparison with init - runlevels


Runlevels are mapped to targets
```
Runlevel    Target              Symlink to
Runlevel 0  runlevel0.target -> poweroff.target
Runlevel 1  runlevel1.target -> rescue.target
Runlevel 2  runlevel2.target -> multi-user.target
Runlevel 3  runlevel3.target -> multi-user.target
Runlevel 4  runlevel4.target -> multi-user.target
Runlevel 5  runlevel5.target -> graphical.target
Runlevel 6  runlevel6.target -> reboot.target
```

* Get current target: `systemctl get-default`
* Set target to (e.g.) `multi-user`: `systemctl set-default multi-user.target`

Setting target just changes symlink `/etc/systemd/system/default.target`
(previously it was defined in /etc/inittab).

You can also reboot and poweroff via `systemctl` (although you don't have to).
```
systemctl poweroff
systemctl reboot
```


### Unit files and how to create one

Listing units
```
systemctl list-units
systemctl list-units -t services
systemctl list-unit-files -a
systemctl list-unit-files -a -t services
```

Unit files are in `/lib/systemd/system` when installed with package and in `/etc/systemd/system` when deployed manualy. Admin can create new unit files in `/etc/systemd/system` or extend or overwrite existing units from `/lib/systemd/system` there (so no need to change what was given from package).

To view content of a unit file either cat the unit file (if you know where it is) or `systemctl cat sshd`.

When changing unit files (adding, removing, alternating), you must call `systemctl daemon-reload`.

Documentation to unit files is in _systemd.service(5)_ so go ahead and `man 5 systemd.service`.

Example workflow:
```
cp /lib/systemd/system/sshd.service \
   /etc/systemd/system/xxx.service
vim /etc/systemd/system/xxx.service
systemctl daemon-reload
systemctl start xxx
journalctl -u xxx -f
systemctl enable xxx
```

### Configuring systemd

There are three main configuration files
* `/etc/systemd/journald.conf`
* `/etc/systemd/system.conf`
* `/etc/systemd/user.conf`

```
/etc/systemd/journald.conf
  mkdir /var/log/journal
  SystemMaxUse=500M
  systemctl restart systemd-journald 
```

## FirewallD

```
systemctl enable firewalld
systemctl start firewalld
firewall-cmd --state
firewall-cmd --get-default
firewall-cmd --list-all
```

## SELinux

## Tips and tricks


```
systemd-analyze
systemd-analyze blame
systemd-analyze critical-chain
systemd-analyze critical-chain expenseit-web
systemctl list-dependencies expenseit-web
systemctl kill expenseit-web
journalctl /usr/sbin/sshd
journalctl -b -1
journalctl --since=today
journalctl -p err
journalctl -u expenseit-web -e -b
journalctl -u expenseit-web -f
systemctl show mysqld | grep CPUShares
systemctl suspend
systemctl hibernate
```

```
hostnamectl
localectl
timedatectl
loginctl
```




## Random links

### Digitalocean articles

https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files
https://www.digitalocean.com/community/tutorials/initial-server-setup-with-centos-7
https://www.digitalocean.com/community/tutorials/systemd-essentials-working-with-services-units-and-the-journal

### Red Hat official documentation

https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/index.html
https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/
https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/chap-Managing_Services_with_systemd.html

### Other

* Systemd conference: https://systemd.events/
* Great video about SELinux: https://www.youtube.com/watch?v=MxjenQ31b70
* More about Atomic Host: https://access.redhat.com/articles/rhel-atomic-getting-started
* Forwarding journald logs:
  * http://www.freedesktop.org/software/systemd/man/systemd-journal-upload.html
  * http://www.freedesktop.org/software/systemd/man/systemd-journal-remote.html
